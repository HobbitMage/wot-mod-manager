﻿; World of Tanks Mod Pack install script.
; See license.rtf for the full license.

#define MyAppName "WoT Mod Pack"
#define MyAppVersion "0.8.6.2"
#define MyAppPublisher "Skobkin.ru"
#define MyAppURL "http://skobkin.ru/wot-mod-pack/"
#define MyAppVkURL "http://vk.com/wot_mod_pack"

; Current Mods Path
#define ModsPath "res_mods\0.8.6"

#define WoTAppID "1EAC1D02-C6AC-4FA6-9A44-96258C37C812"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppID={{2A7906CB-34E3-4AD0-BE0D-28F7307B3E19}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={code:GetDefaultPath}
;DefaultDirName=World_of_Tanks
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
LicenseFile=license-English.txt
OutputDir=installer
OutputBaseFilename=wmp_{#MyAppVersion}
SetupIconFile=wot.ico
Compression=lzma2/Ultra64
SolidCompression=true
Uninstallable=true
UsePreviousAppDir=true
CreateAppDir=true
AppendDefaultDirName=false
EnableDirDoesntExistWarning=true
DirExistsWarning=no
InternalCompressLevel=Ultra64
DisableWelcomePage=true
DisableReadyPage=true
AlwaysShowDirOnReadyPage=true
DisableFinishedPage=true
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany={#MyAppPublisher}
VersionInfoDescription=World of Tanks mod collection
VersionInfoCopyright=© {#MyAppPublisher}
VersionInfoProductName={#MyAppName}
UninstallDisplayName={#MyAppName}
AppCopyright=© Skobkin.ru
AppVerName={#MyAppVersion}
UninstallDisplayIcon={app}\wot.ico

[Types]
Name: recommended; Description: Recommended; 
Name: custom; Description: Custom installation; Flags: iscustom

[Components]
Name: interface; Description: "Модификации интерфейса"; Flags: checkablealone; 
Name: interface\battle; Description: "Боевой интерфейс"; 

; XVM
Name: interface\battle\XVM; Description: "XVM (OTM, иконки, часы)"; Types: recommended; 
Name: interface\battle\XVM\main; Description: "Основной боевой интерфейс, маркеры"; Types: recommended;
Name: interface\battle\XVM\battleloading; Description: "Загрузка боя (иконки, часы)"; Types: recommended;
Name: interface\battle\XVM\playerspanel; Description: "Панели игроков (иконки)"; Types: recommended;
; Захват зависит от ушей:
Name: interface\battle\XVM\playerspanel\teambasespanel; Description: "Панель захвата базы"; Types: recommended;
;Name: interface\battle\XVM\finalstatistic; Description: "Финальная статистика"; Types: recommended;

; XVM Icons
Name: interface\battle\XVM\clanicons; Description: "Иконки кланов и игроков"; Types: recommended;

; XVM Config
Name: interface\battle\XVM\conf; Description: "Настройки"; Types: recommended; 
Name: interface\battle\XVM\conf\Skobkin; Description: "Конфиг от Skobkin.ru"; Types: recommended; Flags: exclusive;
Name: interface\battle\XVM\conf\Skobkin_censored; Description: "Конфиг от Skobkin.ru (цензурный)"; Flags: exclusive; 


; Damage Panels
Name: interface\battle\DamagePanel; Description: "DamagePanel. Отображение полученного урона.";
; 1 CDP by STL1Te (fix by poVitter)
Name: interface\battle\DamagePanel\sdp; Description: "SDP (Dellux, STL1te)"; Flags: exclusive; Types: recommended; 
Name: interface\battle\DamagePanel\sdp\cfg_skobkin; Description: "Конфиг от Skobkin.ru"; Flags: exclusive; Types: recommended;
Name: interface\battle\DamagePanel\sdp\cfg_mini; Description: "Конфиг Mini (компактный)"; Flags: exclusive;
Name: interface\battle\DamagePanel\sdp\cfg_default; Description: "Конфиг по умолчанию"; Flags: exclusive; 


; Damage Indicators
Name: interface\battle\DamageIndicator; Description: "Индикатор направления урона";
; 1
Name: interface\battle\DamageIndicator\1; Description: "#1. Фигурный индикатор (poVitter)"; Flags: exclusive; Types: recommended; 


; Chat
Name: interface\battle\chat; Description: "Чат";
; 1
Name: interface\battle\chat\1; Description: "#1. Прозрачный чат"; Flags: exclusive; Types: recommended; 


; Minimap
Name: interface\battle\minimap; Description: "Миникарта";
; 1
Name: interface\battle\minimap\1; Description: "#1. Линия направления огня"; Flags: exclusive;
; 2
Name: interface\battle\minimap\2; Description: "#2. Линия направления огня"; Flags: exclusive;
; Enhanced HD Minimaps Gen. 2
Name: interface\battle\minimap\ehdmmg2; Description: "#3. Enhanced HD Minimaps Gen. 2"; Flags: exclusive; Types: recommended;
; Config default
Name: interface\battle\minimap\ehdmmg2\cfg_default; Description: "Конфиг по умолчанию"; Flags: exclusive;
; Config skobkin 
Name: interface\battle\minimap\ehdmmg2\cfg_skobkin; Description: "Конфиг от Skobkin.ru"; Flags: exclusive; Types: recommended;

; Crosshairs
Name: interface\battle\Crosshairs; Description: "Измененные прицелы";
; skobkin
Name: interface\battle\Crosshairs\skobkin; Description: "skobkin #1. Время перезарядки. White"; Flags: exclusive; Types: recommended;
; STL1te #1
Name: interface\battle\Crosshairs\stl1te1; Description: "STL1te #1. Время перезарядки. White"; Flags: exclusive;
; STL1te #2 
Name: interface\battle\Crosshairs\stl1te2; Description: "STL1te #2. Время перезарядки. Green"; Flags: exclusive;
; STL1te #3 
Name: interface\battle\Crosshairs\stl1te3; Description: "STL1te #3. Время перезарядки. Vspishka."; Flags: exclusive;

; STL1te white crosshair
Name: interface\battle\Crosshairs\sniper; Description: "STL1te. Белое сведение";
Name: interface\battle\Crosshairs\reduction\stl1te1; Description: "1"; Flags: exclusive; Types: recommended; 
Name: interface\battle\Crosshairs\reduction\stl1te2; Description: "2"; Flags: exclusive;


; Traverse
Name: interface\battle\Crosshairs\traverse; Description: "Углы горизонтальной наводки"; Types: recommended; 


; Color frag correlation on the top of the screen
;Name: interface\battle\frag_correlation; Description: "Цветная корреляция фрагов команд";


; Scope shadow off // replaced by interface\battle\server_cross\scope_shadow_off mod
;Name: interface\battle\ScopeShadowOff; Description: "Отключение затемнения в снайперском режиме"; Types: recommended;


; Zoom mod 
Name: interface\battle\zoom; Description: "Zoom и серверный прицел.";
; No Scroll
Name: interface\battle\zoom\no_scroll; Description: "Отключить переключение колесом мыши"; Types: recommended;
; Zoom + Artillery Zoom
Name: interface\battle\zoom\zoom_artzoom; Description: "Zoom + арт. Zoom"; Types: recommended;
; Sniper Zoom X4
Name: interface\battle\zoom\sniper_zoom_x4; Description: "Снайперский зум X4"; Types: recommended;

; Server Cross
Name: interface\battle\server_cross; Description: "Серверный прицел";
Name: interface\battle\server_cross\scope_shadow_off; Description: "Выключение затемнения в прицеле"; Flags: exclusive;
Name: interface\battle\server_cross\scope_shadow_on; Description: "Затемнение в прицеле"; Flags: exclusive;

; Hangar
Name: interface\hangar; Description: "Ангар";
; Garage 2 rowed carousel
Name: interface\hangar\2row_carousel; Description: "Танки в два ряда"; Types: recommended;

; Other 
Name: interface\other; Description: "Другие модификации"; 

; Statistic mods
Name: interface\other\stat; Description: "Моды статистики";

; Configs
; 1 - damage
;Name: interface\other\stat\damage; Description: "Сортировка по урону"; Types: recommended; Flags: exclusive;
; 2 - exp
;Name: interface\other\stat\exp; Description: "Сортировка по опыту"; Flags: exclusive;

; Achievements 
;Name: interface\other\stat\achievements; Description: "Достижения"; Types: recommended;

; Damage average and frags average
Name: interface\other\stat\userinfo; Description: "Статистика юзера (средний урон)"; Types: recommended;

; FinalStatistic: Damage in command statistics
Name: interface\other\stat\finalstatistic; Description: "Командная статистика"; Types: recommended; 

; ServiceChannelPage: session statistics
Name: interface\other\stat\servicechannelpage; Description: "Статистика за сессию"; Types: recommended; 


; Video logo off
Name: interface\other\video_logo; Description: "Отключение заставки при запуске"; Types: recommended;

; Tech tree
Name: interface\other\tech_tree; Description: "Компактное дерево исследований"; Types: recommended;

; Textures
Name: textures; Description: "Текстуры";
Name: textures\tanks; Description: "Танки";


; Белые трупы
Name: textures\tanks\white_crash; Description: "AHuMex. Белые трупы";


; Camouflage
Name: textures\tanks\camo; Description: "Камуфляжи (для шкурок и разгрузки)";
Name: textures\tanks\camo\no_camo; Description: "AHuMex. Отключение"; Flags: exclusive;
;Name: textures\tanks\camo\camo_35p; Description: "Прозрачные камуфляжи (35%)"; Flags: exclusive;

; Отключение деколей
Name: textures\tanks\no_decals; Description: "AHuMex. Отключение деколей"; 


; Графика
Name: graphics; Description: "Графика";
Name: graphics\shaders; Description: "Шейдеры"; 
Name: graphics\shaders\no_bloom; Description: "Отключение Bloom от DraX";  Types: recommended;
;Name: graphics\visibility; Description: "Видимость";
; Мод в стадии теста: http://forum.worldoftanks.ru/index.php?/topic/534353- включить, когда будет стабилен
;Name: graphics\visibility\no_fog; Description: "Увеличение дальности обзора";

; Links
Name: links; Description: "Ссылки в меню Пуск";
Name: links\wmp_url; Description: "Ссылка на страницу Wot Mod Pack"; Types: recommended; 
Name: links\wmp_vk_url; Description: "Ссылка на WoT Mod Pack ВКонтакте"; Types: recommended; 

[Languages]
Name: russian; MessagesFile: compiler:Languages\Russian.isl; LicenseFile: license-Russian.txt; 
Name: english; MessagesFile: compiler:Default.isl; LicenseFile: license-English.txt; 
Name: ukrainian; MessagesFile: compiler:Languages\Ukrainian.isl; LicenseFile: license-Ukrainian.txt; 

[Files]
; Zoom Mods

; Sniper Zoom X4
;interface\battle\zoom\sniper_zoom_x4

; Common script
Source: mods\zoom\common\res_mods\wot_version\scripts\client\AvatarInputHandler\DynamicCameras\*; DestDir: {app}\{#ModsPath}\scripts\client\AvatarInputHandler\DynamicCameras; Components: interface\battle\zoom\zoom_artzoom interface\battle\zoom\no_scroll; 
; Sniper Zoom X4
Source: mods\zoom\zoom_x4\res_mods\wot_version\gui\ZoomX4.xml; DestDir: {app}\{#ModsPath}\gui; Components: interface\battle\zoom\sniper_zoom_x4;
; Zoom + Artillery zoom
Source: mods\zoom\zoom_artzoom\res_mods\wot_version\gui\avatar_input_handler.xml; DestDir: {app}\{#ModsPath}\gui; Components: interface\battle\zoom\zoom_artzoom; 
; No Scroll
Source: mods\zoom\no_scroll\res_mods\wot_version\gui\NoScroll.xml; DestDir: {app}\{#ModsPath}\gui; Components: interface\battle\zoom\no_scroll; 


; Server Cross
Source: mods\server_crosshair\res_mods\wot_version\*; DestDir: {app}\{#ModsPath}; Flags: recursesubdirs createallsubdirs; Components: interface\battle\server_cross;
; Scope Shadow Off
Source: mods\server_crosshair\config\scope_shadow_off\ServerCross.xml; DestDir: {app}\{#ModsPath}\gui; Components: interface\battle\server_cross\scope_shadow_off; 
; Scope Shadow On
Source: mods\server_crosshair\config\scope_shadow_on\ServerCross.xml; DestDir: {app}\{#ModsPath}\gui; Components: interface\battle\server_cross\scope_shadow_on; 



; XVM 
Source: mods\xvm\res_mods\wot_version\gui\flash\battle.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\main; 
Source: mods\xvm\res_mods\wot_version\gui\flash\VehicleMarkersManager.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\main; 
Source: mods\xvm\res_mods\wot_version\gui\flash\StatisticForm.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\main; 
Source: mods\xvm\res_mods\wot_version\gui\flash\TeamBasesPanel.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\playerspanel\teambasespanel;
;Source: mods\xvm\res_mods\wot_version\gui\flash\FinalStatistic.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\finalstatistic;
Source: mods\xvm\res_mods\wot_version\gui\flash\battleloading.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\battleloading; 
Source: mods\xvm\res_mods\wot_version\gui\flash\PlayersPanel.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\playerspanel; 

; XVM Icons
Source: mods\xvm\res_mods\clanicons\*; DestDir: {app}\res_mods\clanicons; Flags: recursesubdirs createallsubdirs; Components: interface\battle\XVM\clanicons;

; XVM my config 
Source: mods\xvm\config\skobkin\XVM.xvmconf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\conf\Skobkin; 

; XVM my censored config 
Source: mods\xvm\config\skobkin_censored\XVM.xvmconf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\XVM\conf\Skobkin_censored;


; Scope shadow off // replaced by interface\battle\server_cross\scope_shadow_off
;Source: mods\scope_shadow\res_mods\wot_version\gui\flash\ScopeShadow.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\ScopeShadowOff; 


; Damage Panel
; SDP (Dellux, STL1te)
Source: mods\sdp\res_mods\wot_version\gui\flash\DamagePanel.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\DamagePanel\sdp; 

; Configs
; Skobkin.ru
Source: mods\sdp\config\skobkin\SDPSetting.xml; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\DamagePanel\sdp\cfg_skobkin;

; Mini
Source: mods\sdp\config\mini\SDPSetting.xml; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\DamagePanel\sdp\cfg_mini;

; Default
Source: mods\sdp\config\default\SDPSetting.xml; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\DamagePanel\sdp\cfg_default;
 

; Damage Indicators
; 1
Source: mods\damage_indicator\res_mods\wot_version\gui\flash\DamageIndicator.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\DamageIndicator\1;


; Chat
; 1
Source: mods\transparent_chat\res_mods\wot_version\gui\flash\BattleMessenger.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\chat\1;


; Minimap
; 1
Source: mods\minimap_direction1\res_mods\wot_version\gui\flash\Minimap.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\minimap\1;
; 2
Source: mods\minimap_direction2\res_mods\wot_version\gui\flash\Minimap.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\minimap\2;
; Enhanced HD Minimaps Gen. 2
Source: mods\hd_minimap_2nd_gen\res_mods\wot_version\gui\*; DestDir: {app}\{#ModsPath}\gui; Flags: recursesubdirs createallsubdirs; Components: interface\battle\minimap\ehdmmg2;
Source: mods\hd_minimap_2nd_gen\res_mods\wot_version\scripts\*; DestDir: {app}\{#ModsPath}\scripts; Flags: recursesubdirs createallsubdirs; Components: interface\battle\minimap\ehdmmg2;
; Default config
Source: mods\hd_minimap_2nd_gen\config\default\Mmap.xml; DestDir: {app}\res_mods; Components: interface\battle\minimap\ehdmmg2\cfg_default;
; Skobkin config
Source: mods\hd_minimap_2nd_gen\config\skobkin\Mmap.xml; DestDir: {app}\res_mods; Components: interface\battle\minimap\ehdmmg2\cfg_skobkin;

; TimerCrosshair
Source: mods\timer_crosshair\skobkin\res_mods\wot_version\gui\flash\*; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\Crosshairs\skobkin; 
Source: mods\timer_crosshair\stl1te_std_white\res_mods\wot_version\gui\flash\*; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\Crosshairs\stl1te1; 
Source: mods\timer_crosshair\stl1te_std_green\res_mods\wot_version\gui\flash\*; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\Crosshairs\stl1te2; 
Source: mods\timer_crosshair\stl1te_vspishka\res_mods\wot_version\gui\flash\*; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\Crosshairs\stl1te3;


; White Sniper
Source: mods\timer_crosshair\stl1te_reduction1\res_mods\wot_version\gui\flash\*; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\Crosshairs\reduction\stl1te1; 
Source: mods\timer_crosshair\stl1te_reduction2\res_mods\wot_version\gui\flash\*; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\Crosshairs\reduction\stl1te2; 


; Traverse
Source: mods\traverse\res_mods\wot_version\gui\flash\*; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\Crosshairs\traverse; 


; interface\battle\frag_correlation
;Source: mods\frag_correlation\res_mods\wot_version\gui\flash\FragCorrelation.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\battle\frag_correlation;

; Hangar
; Garage 2 rowed carousel
Source: mods\2_row_carousel\res_mods\wot_version\gui\*; DestDir: {app}\{#ModsPath}\gui; Flags: recursesubdirs createallsubdirs; Components: interface\hangar\2row_carousel;

; Textures

; AHuMex. Белые трупы
Source: mods\white_crash\res_mods\wot_version\vehicles\*_crash.dds; DestDir: {app}\{#ModsPath}\vehicles; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\white_crash;
Source: mods\white_crash\res_mods\wot_version\content\*.dds; DestDir: {app}\{#ModsPath}\content; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\white_crash;

; AHuMex. Отключение камуфляжа
Source: mods\no_camo_decals\res_mods\wot_version\vehicles\*_CM.dds; DestDir: {app}\{#ModsPath}\vehicles; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\camo\no_camo;

; 35% camouflage
;Source: res\vehicles\american\Camouflage\*.dds; DestDir: {app}\{#ModsPath}\vehicles\american\Camouflage; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\camo\camo_35p;
;Source: res\vehicles\british\Camouflage\*.dds; DestDir: {app}\{#ModsPath}\vehicles\british\Camouflage; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\camo\camo_35p;
;Source: res\vehicles\chinese\Camouflage\*.dds; DestDir: {app}\{#ModsPath}\vehicles\chinese\Camouflage; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\camo\camo_35p;
;Source: res\vehicles\french\Camouflage\*.dds; DestDir: {app}\{#ModsPath}\vehicles\french\Camouflage; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\camo\camo_35p;
;Source: res\vehicles\german\Camouflage\*.dds; DestDir: {app}\{#ModsPath}\vehicles\german\Camouflage; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\camo\camo_35p;
;Source: res\vehicles\russian\Camouflage\*.dds; DestDir: {app}\{#ModsPath}\vehicles\russian\Camouflage; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\camo\camo_35p;


; AHuMex. Отключение деколей
Source: mods\no_camo_decals\res_mods\wot_version\gui\maps\vehicles\decals\*; DestDir: {app}\{#ModsPath}\gui\maps\vehicles\decals; Flags: recursesubdirs createallsubdirs; Components: textures\tanks\no_decals;


; Graphics

; graphics\shaders\no_bloom
Source: mods\no_bloom\res_mods\wot_version\shaders\hdr\wg_hdr_bloom.*.fxo; DestDir: {app}\{#ModsPath}\shaders\hdr; Components: graphics\shaders\no_bloom; 

; graphics\visibility\no_fog
;Source: res\spaces\*; DestDir: {app}\{#ModsPath}\spaces; Flags: recursesubdirs createallsubdirs; Components: graphics\visibility\no_fog;


; Other

; Video logo off
Source: mods\video_logo_off\res_mods\wot_version\gui\flash\video\Logo_All.usm; DestDir: {app}\{#ModsPath}\gui\flash\video; Components: interface\other\video_logo; 


; Statistic mods
; Configs
; 1 - damage 
;Source: res\gui\flash\statistic\cfg\1\UserInfo.xml; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\other\stat\damage;

; 2 - exp
;Source: res\gui\flash\statistic\cfg\2\UserInfo.xml; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\other\stat\exp;

; Achievements
;Source: res\gui\flash\statistic\Achievements.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\other\stat\achievements;

; UserInfo
Source: mods\statistic\res_mods\wot_version\gui\flash\UserInfo.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\other\stat\userinfo;

; FinalStatistics
Source: mods\statistic\res_mods\wot_version\gui\flash\FinalStatistic.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\other\stat\finalstatistic;

; ServiceChannelPage
Source: mods\statistic\res_mods\wot_version\gui\flash\ServiceChannelPage.swf; DestDir: {app}\{#ModsPath}\gui\flash; Components: interface\other\stat\servicechannelpage;


; Tech tree
Source: mods\techtree\res_mods\wot_version\gui\flash\techtree\tree-shared.xml; DestDir: {app}\{#ModsPath}\gui\flash\techtree; Components: interface\other\tech_tree;


; WoT icon
Source: wot.ico; DestDir: {app}; 

[Icons]
Name: "{group}\Официальный сайт"; Filename: {#MyAppURL}; Components: links\wmp_url; Comment: "WoT Mod Pack на Skobkin.ru"; IconFilename: wot.ico; Flags: excludefromshowinnewinstall;
Name: "{group}\Страница ВКонтакте"; Filename: {#MyAppVkURL}; Components: links\wmp_vk_url; Comment: "Страница WoT Mod Pack ВКонтакте"; IconFilename: vk.ico; Flags: excludefromshowinnewinstall;
Name: "{group}\Удалить WMP"; Filename: {uninstallexe}; IconFilename: {app}\wot.ico; Flags: excludefromshowinnewinstall;
 


[Code]
function NextButtonClick(CurPageID: Integer): Boolean;
begin
    if(CurPageID = wpSelectDir) then
    begin
        if(FileExists(ExpandConstant('{app}') + '\WorldOfTanks.exe')) then
        begin
            Result := True;
        end
        else begin
            MsgBox('Выберите корневую директорию WorldOfTanks'#10#13'Например: C:\Games\World_of_Tanks\' , mbInformation, MB_OK);
        end;
    end
    else
    begin
      Result := True;
    end; 
end;

// Search WoT path
function GetInstallPath(): string;
var
  WoTPath: string;
  sRegAppUninstKey: array [1..2] of string;
  aKeys: array [1..2] of Integer;
  aKeyEnds: array [1..3] of string;
  i,j, h: Integer;
begin
  Result  := '';
  WoTPath := '';
  sRegAppUninstKey[1] := 'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812}';
  sRegAppUninstKey[2] := 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812}';
  
  aKeys[1] := HKEY_CURRENT_USER;
  aKeys[2] := HKEY_LOCAL_MACHINE;

  aKeyEnds[1] := '';
  aKeyEnds[2] := '_is1';
  aKeyEnds[3] := '_is0';

  for i := 1 to 2 do
  for j := 1 to 3 do
  for h := 1 to 2 do
  begin
    if (WoTPath = '') and RegKeyExists(aKeys[i], sRegAppUninstKey[h] + aKeyEnds[j]) then
    try
      if not RegQueryStringValue(aKeys[i], sRegAppUninstKey[h] + aKeyEnds[j], 'InstallLocation', WoTPath) or
         not DirExists(WoTPath) then // or WriteAccessDenied(WoTPath)
        WoTPath := ''                                
      else
        begin
          //MsgBox('Найден клиент World of Tanks:'#10#13 + WoTPath + #10#13, mbInformation, MB_OK);
          Result := WoTPath;
          Exit;
        end;
    except
    end;//try
  end;//for j
end;

function GetDefaultPath(lol: String): string;
begin
  Result := GetInstallPath();
end;
///Search WoT path


// Site labels
var  
 MouseLabel,SiteLabel: TLabel;  
  
 // Labels actions
 procedure SiteLabelOnClick(Sender: TObject);  
 var  
 ErrorCode: Integer;  
 begin  
 ShellExec('open', 'http://skobkin.ru/wot-mod-pack/', '', '', SW_SHOWNORMAL, ewNoWait, ErrorCode)  
 end;  
 
 procedure SiteLabelMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);  
 begin  
 SiteLabel.Font.Color:=clRed  
 end;  
  
 procedure SiteLabelMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);  
 begin  
 SiteLabel.Font.Color:=clBlue  
 end;  
  
 procedure SiteLabelMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);  
 begin  
 SiteLabel.Font.Color:=clGreen  
 end;  
  
 procedure SiteLabelMouseMove2(Sender: TObject; Shift: TShiftState; X, Y: Integer);  
 begin  
 SiteLabel.Font.Color:=clBlue  
 end;  
 ///Labels actions
 
 procedure InitializeWizard();  
 begin  
 MouseLabel:=TLabel.Create(WizardForm)  
 MouseLabel.Width:=WizardForm.Width  
 MouseLabel.Height:=WizardForm.Height  
 MouseLabel.Autosize:=False  
 MouseLabel.Transparent:=True  
 MouseLabel.OnMouseMove:=@SiteLabelMouseMove2  
 MouseLabel.Parent:=WizardForm  
  
 SiteLabel:=TLabel.Create(WizardForm)  
 SiteLabel.Left:=10  
 SiteLabel.Top:=330  
 SiteLabel.Cursor:=crHand  
 SiteLabel.Font.Color:=clBlue  
 SiteLabel.Caption:='Страница WoT Mod Pack'  
 SiteLabel.OnClick:=@SiteLabelOnClick  
 SiteLabel.OnMouseDown:=@SiteLabelMouseDown  
 SiteLabel.OnMouseUp:=@SiteLabelMouseUp  
 SiteLabel.OnMouseMove:=@SiteLabelMouseMove  
 SiteLabel.Parent:=WizardForm  
end;  
///Site labels

[Messages]
; English by Mage
english.LicenseAccepted=Affirmative!
english.LicenseNotAccepted=Negative!
english.ButtonCancel=Surrender
english.ButtonBack=Retreat!
english.ButtonNext=Charge!
english.BrowseDialogLabel=Select lodgement for attack and press OK.
english.BrowseDialogTitle=Select lodgement
english.ButtonNewFolder=Fortify
english.ButtonNo=Negative!
english.ButtonYes=Affirmative!
english.ConfirmUninstall=Do you seriously want to delete this great %1?
english.DiskSpaceMBLabel=You need [mb] MB slots in hangar.
english.ErrorCopying=Error with file landing:
english.ErrorCreatingDir=Error when preparing lodgement %1
english.ExitSetupMessage=Deployment of troops was not been completed. If you retreat, operation would be failed!%n%nYou can restart operation by executing installer.%n%nRetreat?
english.ExitSetupTitle=Retreat!
english.InstallingLabel=Please wait, [name] is performing an attack on your computer.
english.LicenseLabel=Take the oath before proceeding.
english.LicenseLabel3=Read the text of the solemn oath. You have to take the oath before starting the attack.
english.NewFolderName=New lodgement
english.PathLabel=Lodgement:
english.PreparingDesc=[name] is preparing to assault your computer.
english.SelectComponentsDesc=Select forces to land
english.SelectComponentsLabel2=Select forces to land. If you a ready, click "Charge!"
english.SelectDirBrowseLabel=To continue the landing, click "Charge!". If you want to select another foothold, click "Scouting".
english.SelectDirDesc=Where to land [name]?
english.SelectDirLabel3=[name] will be landed at:
english.SelectLanguageLabel=Choose a language for the installation.
english.SetupAborted=The landing failed.%n%nSolve the problem and try again.
english.SetupAppTitle=Landing
english.ButtonBrowse=Scouting...
english.SetupFileCorrupt=You are misinformed. Please download the installation package again.
english.SetupWindowTitle=Landing %1
english.StatusExtractFiles=Landing...
english.StatusUninstalling=Retreating %1...
english.WizardInstalling=Landing
english.WizardLicense=The oath
english.WizardSelectComponents=Select forces
english.WizardSelectDir=Select lodgement
english.ComponentsDiskSpaceMBLabel=The current set of technics requires at least [mb] MB in the hangar.
english.ButtonWizardBrowse=Scouting...

; Русский язык
russian.LicenseAccepted=Так точно!
russian.LicenseNotAccepted=Никак нет!
russian.ButtonCancel=Капитуляция
russian.ButtonBack=Отступаем!
russian.ButtonNext=В атаку!
russian.BrowseDialogLabel=Выберите плацдарм для наступления и нажмите OK.
russian.BrowseDialogTitle=Выбор плацдарма
russian.ButtonNewFolder=Возвести укрепления
russian.ButtonNo=Никак нет!
russian.ButtonYes=Так точно!
russian.ConfirmUninstall=Вы уверены, что хотите удалить замечательный %1?
russian.DiskSpaceMBLabel=Необходимо [mb] MB места в ангаре.
russian.ErrorCopying=Ошибка при десантировании файла:
russian.ErrorCreatingDir=Невозможно подготовить плацдарм %1
russian.ExitSetupMessage=Размещение войск не завершено. Если вы отступите сейчас, наступление будет провалено!%n%nВы можете начать наступление заново запустив установочный файл.%n%nОтступить?
russian.ExitSetupTitle=Отступление!
russian.InstallingLabel=Ожидайте, происходит наступление [name] на ваш компьютер.
russian.LicenseLabel=Примите присягу перед тем как продолжить.
russian.LicenseLabel3=Ознакомьтесь с текстом торжественной клятвы. Вы должны принять Присягу перед тем как начать наступление.
russian.NewFolderName=Новый плацдарм
russian.PathLabel=Плацдарм:
russian.PreparingDesc=[name] готовится к наступлению на ваш компьютер.
russian.SelectComponentsDesc=Выберите войска для высадки
russian.SelectComponentsLabel2=Выберите войска, которые вы хотите отправить на высадку. Если вы готовы, нажмите "В атаку!".
russian.SelectDirBrowseLabel=Для продолжения высадки нажмите "В атаку!". Если вы хотите выбрать другой плацдарм, нажмите "Разведка".
russian.SelectDirDesc=Куда необходимо высадить [name]?
russian.SelectDirLabel3=[name] будет высажен в:
russian.SelectLanguageLabel=Выберите язык для установки. Рекомендую выбрать русский :)
russian.SetupAborted=Высадка не удалась.%n%nИсправьте проблемы и попробуйте снова.
russian.SetupAppTitle=Высадка
russian.ButtonBrowse=Разведка...
russian.SetupFileCorrupt=Вас дезинформировали. Пожалуйста, скачайте установочный пакет еще раз.
russian.SetupWindowTitle=Высадка %1
russian.StatusExtractFiles=Высадка...
russian.StatusUninstalling=Отступление %1...
russian.WizardInstalling=Высадка
russian.WizardLicense=Присяга
russian.WizardSelectComponents=Выбор войск
russian.WizardSelectDir=Выбор плацдарма
russian.ComponentsDiskSpaceMBLabel=Текущий набор техники требует не менее [mb] MB в ангаре.
russian.ButtonWizardBrowse=Разведка...

; Українська мова by Codkelden
ukrainian.LicenseAccepted=Так точно!
ukrainian.LicenseNotAccepted=Ніяк ні!
ukrainian.ButtonCancel=Капітуляція
ukrainian.ButtonBack=Відступаємо!
ukrainian.ButtonNext=В атаку!
ukrainian.BrowseDialogLabel=Оберіть плацдарм для наступу та натисніть OK.
ukrainian.BrowseDialogTitle=Вибір плацдарму
ukrainian.ButtonNewFolder=Побудувати укріплення
ukrainian.ButtonNo=Ніяк ні!
ukrainian.ButtonYes=Так точно!
ukrainian.ConfirmUninstall=Вы впевнені, що хочете видалити чудовий %1?
ukrainian.DiskSpaceMBLabel=Необхідно [mb] MB місця в ангарі.
ukrainian.ErrorCopying=Помилка при десантуванні файлу:
ukrainian.ErrorCreatingDir=Неможливо підготувати плацдарм %1
ukrainian.ExitSetupMessage=Розміщення військ не завершено. Якщо ви відступите зараз, наступ буде провалено!%n%nВы можете почати наступ заново запустив установочный файл.%n%nВідступити?
ukrainian.ExitSetupTitle=Відступаємо!
ukrainian.InstallingLabel=Зачекайте, триває наступ [name] на ваш комп'ютер.
ukrainian.LicenseLabel=Прийміть Присягу перед тим, як продовжити.
ukrainian.LicenseLabel3=Ознайомьтесь із текстом Присяги. Ви повинні скласти Присягу перед тим, як почати наступ.
ukrainian.NewFolderName=Новий плацдарм
ukrainian.PathLabel=Плацдарм:
ukrainian.PreparingDesc=[name] готується до наступу на ваш комп'ютер.
ukrainian.SelectComponentsDesc=Оберіть війска для висадки
ukrainian.SelectComponentsLabel2=Оберіть війска, які ви хочете висадити. Якщо ви готові, натисніть "В атаку!".
ukrainian.SelectDirBrowseLabel=Для продовження висадки натисніть "В атаку!". Якщо ви хочете обрати інший плацдарм, натисніть "Розвідка".
ukrainian.SelectDirDesc=Куди необхідно висадити [name]?
ukrainian.SelectDirLabel3=[name] буде висажено до:
ukrainian.SelectLanguageLabel=Оберіть мову для установки. Рекомендую обрати українську :)
ukrainian.SetupAborted=Высадка не удалась.%n%nИсправьте проблемы и попробуйте снова.
ukrainian.SetupAppTitle=Висадка
ukrainian.ButtonBrowse=Розвідка...
ukrainian.SetupFileCorrupt=Вас було дезінформовано. Будь ласка, завантажте пакет установки ще раз.
ukrainian.SetupWindowTitle=Висадка %1
ukrainian.StatusExtractFiles=Висадка...
ukrainian.StatusUninstalling=Відступ %1...
ukrainian.WizardInstalling=Висадка
ukrainian.WizardLicense=Присяга
ukrainian.WizardSelectComponents=Вибор військ
ukrainian.WizardSelectDir=Вибір плацдарму
ukrainian.ComponentsDiskSpaceMBLabel=Поточний набір техніки потребує не менш, ніж [mb] MB в ангарі.
ukrainian.ButtonWizardBrowse=Розвідка...
